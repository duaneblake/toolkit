var 
    gulp         = require('gulp'),
    postcss      = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    sass         = require('gulp-sass'),
    sourcemaps   = require('gulp-sourcemaps')
    rename       = require('gulp-rename'),

    browserify = require('browserify'),
    babelify   = require('babelify'),
    source     = require('vinyl-source-stream'),
    buffer     = require('vinyl-buffer'),
    uglify     = require('gulp-uglify')
    // webpack      = require('webpack-stream')
;

var
    jsSRC    = 'wmp.js',
    jsFolder = 'wmp-frontend/',
    // jsDist = 'wmp-frontend/',
    // jsWatch = 'src/js',
    jsFiles  = [jsSRC]
;

//The css for devlopment creates a wmp.css
gulp.task('css-build', function () {
    return gulp.src('wmp-frontend/wmp.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle:'compact'}))
        .pipe(sourcemaps.write())
        .pipe(postcss([
            require('autoprefixer')
        ])
        .pipe(gulp.dest('css'))
    );
});

//The css for production  creates a wmp.min.css
gulp.task('css-prod', function () {
    return gulp.src('wmp-frontend/wmp.scss')
        .pipe(sass({outputStyle:'compressed'}))
        .pipe(rename({
            extname: ".min.css"
          }))
        .pipe(postcss([
            require('autoprefixer')
        ])
        .pipe(gulp.dest('css'))
    );
});

gulp.task('js-build', function (done) {
    jsFiles.map(function (entry) {
        return browserify({
            entries: [jsFolder +  entry]
        })
        .transform(babelify, {presets: ['@babel/env']})
        .bundle()
        .pipe(source(entry))
        .pipe(rename({ extname: '.min.js'}))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('js'))
    });
    done();
});

// gulp.task('js-build', function() {
//     return gulp.src('wmp-frontend/wmp.scss')
//     .pipe(webpack({
//         mode: 'production',
//         output: {
//           filename: 'wmp.js',
//         },
//         target: 'web',
//         module: {
//           rules: [
//             {
//               use: {
//                 loader: 'babel-loader',
//                 options: {
//                   presets: ['@babel/preset-env']
//                 }
//               }
//             }
//           ]
//         }
//       }))
//       .pipe(gulp.dest('build/js'))
//       ;
// });

gulp.task('watch', function () {
    gulp.watch(['wmp-frontend/**/*.scss', './wmp-frontend/**/*.js'], gulp.series(['css-build', 'js-build']));
});