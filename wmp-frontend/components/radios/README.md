# Radios

## Guidance

Find out more about the radios component and when to use it in the [WMP digital service manual](https://beta.wmp.uk/service-manual/styles-components-patterns/radios).

## Quick start examples

### Radios

[Preview the radios component](https://wmp.github.io/wmp-frontend/components/radios/index.html)

#### HTML markup

```html
<div class="wmp-form-group">
  <fieldset class="wmp-fieldset" aria-describedby="example-hint">
    <legend class="wmp-fieldset__legend">
      Have you changed your name?
    </legend>
    <span class="wmp-hint" id="example-hint">
    This includes changing your last name or spelling your name differently.
    </span>
    <div class="wmp-radios">
      <div class="wmp-radios__item">
        <input class="wmp-radios__input" id="example-1" name="example" type="radio" value="yes">
        <label class="wmp-label wmp-radios__label" for="example-1">
        Yes
        </label>
      </div>
      <div class="wmp-radios__item">
        <input class="wmp-radios__input" id="example-2" name="example" type="radio" value="no" checked>
        <label class="wmp-label wmp-radios__label" for="example-2">
        No
        </label>
      </div>
    </div>
  </fieldset>
</div>
```

#### Nunjucks macro

```
{% from 'components/radios/macro.njk' import radios %}

{{ radios({
  "idPrefix": "example",
  "name": "example",
  "fieldset": {
    "legend": {
      "text": "Have you changed your name?"
    }
  },
  "hint": {
    "text": "This includes changing your last name or spelling your name differently."
  },
  "items": [
    {
      "value": "yes",
      "text": "Yes"
    },
    {
      "value": "no",
      "text": "No",
      "checked": true
    }
  ]
}) }}
```

---

### Radios inline

[Preview the radios inline component](https://wmp.github.io/wmp-frontend/components/radios/inline.html)

#### HTML markup

```html
<div class="wmp-form-group">
  <fieldset class="wmp-fieldset" aria-describedby="example-hint">
    <legend class="wmp-fieldset__legend">
      Have you changed your name?
    </legend>
    <span class="wmp-hint" id="example-hint">
    This includes changing your last name or spelling your name differently.
    </span>
    <div class="wmp-radios wmp-radios--inline">
      <div class="wmp-radios__item">
        <input class="wmp-radios__input" id="example-1" name="example" type="radio" value="yes">
        <label class="wmp-label wmp-radios__label" for="example-1">
        Yes
        </label>
      </div>
      <div class="wmp-radios__item">
        <input class="wmp-radios__input" id="example-2" name="example" type="radio" value="no" checked>
        <label class="wmp-label wmp-radios__label" for="example-2">
        No
        </label>
      </div>
    </div>
  </fieldset>
</div>
```

#### Nunjucks macro

```
{% from 'components/radios/macro.njk' import radios %}

{{ radios({
  "idPrefix": "example",
  "classes": "wmp-radios--inline",
  "name": "example",
  "fieldset": {
    "legend": {
      "text": "Have you changed your name?"
    }
  },
  "hint": {
    "text": "This includes changing your last name or spelling your name differently."
  },
  "items": [
    {
      "value": "yes",
      "text": "Yes"
    },
    {
      "value": "no",
      "text": "No",
      "checked": true
    }
  ]
}) }}
```

---

### Radios disabled

[Preview the radios disabled component](https://wmp.github.io/wmp-frontend/components/radios/disabled.html)

#### HTML markup

```html
<div class="wmp-form-group">
  <fieldset class="wmp-fieldset" aria-describedby="example-disabled-hint">
    <legend class="wmp-fieldset__legend">
      Have you changed your name?
    </legend>
    <span class="wmp-hint" id="example-disabled-hint">
    This includes changing your last name or spelling your name differently.
    </span>
    <div class="wmp-radios">
      <div class="wmp-radios__item">
        <input class="wmp-radios__input" id="example-disabled-1" name="example-disabled" type="radio" value="yes" disabled>
        <label class="wmp-label wmp-radios__label" for="example-disabled-1">
        Yes
        </label>
      </div>
      <div class="wmp-radios__item">
        <input class="wmp-radios__input" id="example-disabled-2" name="example-disabled" type="radio" value="no" disabled>
        <label class="wmp-label wmp-radios__label" for="example-disabled-2">
        No
        </label>
      </div>
    </div>
  </fieldset>
</div>
```

#### Nunjucks macro

```
{% from 'components/radios/macro.njk' import radios %}

{{ radios({
  "idPrefix": "example-disabled",
  "name": "example-disabled",
  "fieldset": {
    "legend": {
      "text": "Have you changed your name?"
    }
  },
  "hint": {
    "text": "This includes changing your last name or spelling your name differently."
  },
  "items": [
    {
      "value": "yes",
      "text": "Yes",
      "disabled": true
    },
    {
      "value": "no",
      "text": "No",
      "disabled": true
    }
  ]
}) }}
```

---

### Radios with a divider

[Preview the radios with a divider component](https://wmp.github.io/wmp-frontend/components/radios/divider.html)

#### HTML markup

```html
<div class="wmp-form-group">
  <fieldset class="wmp-fieldset">
    <legend class="wmp-fieldset__legend">
      How do you want to sign in?
    </legend>
    <div class="wmp-radios">
      <div class="wmp-radios__item">
        <input class="wmp-radios__input" id="example-divider-1" name="example" type="radio" value="government-gateway">
        <label class="wmp-label wmp-radios__label" for="example-divider-1">
        Use Government Gateway
        </label>
      </div>
      <div class="wmp-radios__item">
        <input class="wmp-radios__input" id="example-divider-2" name="example" type="radio" value="wmp-login">
        <label class="wmp-label wmp-radios__label" for="example-divider-2">
        Use WMP.UK login
        </label>
      </div>
      <div class="wmp-radios__divider">or</div>
      <div class="wmp-radios__item">
        <input class="wmp-radios__input" id="example-divider-4" name="example" type="radio" value="create-account">
        <label class="wmp-label wmp-radios__label" for="example-divider-4">
        Create an account
        </label>
      </div>
    </div>
  </fieldset>
</div>
```

#### Nunjucks macro

```
{% from 'components/radios/macro.njk' import radios %}

{{ radios({
  "idPrefix": "example-divider",
  "name": "example",
  "fieldset": {
    "legend": {
      "text": "How do you want to sign in?"
    }
  },
  "items": [
    {
      "value": "government-gateway",
      "text": "Use Government Gateway"
    },
    {
      "value": "wmp-login",
      "text": "Use WMP.UK login"
    },
    {
      "divider": "or"
    },
    {
      "value": "create-account",
      "text": "Create an account"
    }
  ]
}) }}
```

---

### Radios with hint text on items

[Preview the radios with hint text on items component](https://wmp.github.io/wmp-frontend/components/radios/hint.html)

#### HTML markup

```html
<div class="wmp-form-group">
  <fieldset class="wmp-fieldset">
    <legend class="wmp-fieldset__legend">
      <h1 class="wmp-fieldset__heading">
        How do you want to sign in?
      </h1>
    </legend>
    <div class="wmp-radios">
      <div class="wmp-radios__item">
        <input class="wmp-radios__input" id="gov-1" name="gov" type="radio" value="gateway" aria-describedby="gov-1-item-hint">
        <label class="wmp-label wmp-radios__label" for="gov-1">
        Sign in with Government Gateway
        </label>
        <span class="wmp-hint wmp-radios__hint" id="gov-1-item-hint">
        You&#39;ll have a user ID if you've registered for self-assessment or filed a tax return online before.
        </span>
      </div>
      <div class="wmp-radios__item">
        <input class="wmp-radios__input" id="gov-2" name="gov" type="radio" value="verify" aria-describedby="gov-2-item-hint">
        <label class="wmp-label wmp-radios__label" for="gov-2">
        Sign in with WMP.UK login
        </label>
        <span class="wmp-hint wmp-radios__hint" id="gov-2-item-hint">
        You’ll have an account if you’ve already proved your identity with either Barclays, CitizenSafe, Digidentity, Experian, Post Office, Royal Mail or SecureIdentity.
        </span>
      </div>
    </div>
  </fieldset>
</div>
```

#### Nunjucks macro

```
{% from 'components/radios/macro.njk' import radios %}

{{ radios({
  "idPrefix": "gov",
  "name": "gov",
  "fieldset": {
    "legend": {
      "text": "How do you want to sign in?",
      "isPageHeading": true
    }
  },
  "items": [
    {
      "value": "gateway",
      "text": "Sign in with Government Gateway",
      "hint": {
        "text": "You'll have a user ID if you've registered for self-assessment or filed a tax return online before."
      }
    },
    {
      "value": "verify",
      "text": "Sign in with WMP.UK login",
      "hint": {
        "text": "You’ll have an account if you’ve already proved your identity with either Barclays, CitizenSafe, Digidentity, Experian, Post Office, Royal Mail or SecureIdentity."
      }
    }
  ]
}) }}
```

---

### Radios without fieldset

[Preview the radios without fieldset component](https://wmp.github.io/wmp-frontend/components/radios/without-fieldset.html)

#### HTML markup

```html
<div class="wmp-form-group">
  <div class="wmp-radios">
    <div class="wmp-radios__item">
      <input class="wmp-radios__input" id="colours-1" name="colours" type="radio" value="red">
      <label class="wmp-label wmp-radios__label" for="colours-1">
      Red
      </label>
    </div>
    <div class="wmp-radios__item">
      <input class="wmp-radios__input" id="colours-2" name="colours" type="radio" value="green">
      <label class="wmp-label wmp-radios__label" for="colours-2">
      Green
      </label>
    </div>
    <div class="wmp-radios__item">
      <input class="wmp-radios__input" id="colours-3" name="colours" type="radio" value="blue">
      <label class="wmp-label wmp-radios__label" for="colours-3">
      Blue
      </label>
    </div>
  </div>
</div>
```

#### Nunjucks macro

```
{% from 'components/radios/macro.njk' import radios %}

{{ radios({
  "name": "colours",
  "items": [
    {
      "value": "red",
      "text": "Red"
    },
    {
      "value": "green",
      "text": "Green"
    },
    {
      "value": "blue",
      "text": "Blue"
    }
  ]
}) }}
```

---

### Radios with hint text and error message

[Preview the radios with hint text and error message component](https://wmp.github.io/wmp-frontend/components/radios/hint-error.html)

#### HTML markup

```html
<div class="wmp-form-group wmp-form-group--error">
  <fieldset class="wmp-fieldset app-fieldset--custom-modifier" aria-describedby="example-hint example-error" data-attribute="value" data-second-attribute="second-value">
    <legend class="wmp-fieldset__legend">
      Have you changed your name?
    </legend>
    <span class="wmp-hint" id="example-hint">
    This includes changing your last name or spelling your name differently.
    </span>
    <span id="example-error" class="wmp-error-message">
    Please select an option
    </span>
    <div class="wmp-radios">
      <div class="wmp-radios__item">
        <input class="wmp-radios__input" id="example-1" name="example" type="radio" value="yes">
        <label class="wmp-label wmp-radios__label" for="example-1">
        Yes
        </label>
      </div>
      <div class="wmp-radios__item">
        <input class="wmp-radios__input" id="example-2" name="example" type="radio" value="no" checked>
        <label class="wmp-label wmp-radios__label" for="example-2">
        No
        </label>
      </div>
    </div>
  </fieldset>
</div>
```

#### Nunjucks macro

```
{% from 'components/radios/macro.njk' import radios %}

{{ radios({
  "idPrefix": "example",
  "name": "example",
  "errorMessage": {
    "text": "Please select an option"
  },
  "fieldset": {
    "classes": "app-fieldset--custom-modifier",
    "attributes": {
      "data-attribute": "value",
      "data-second-attribute": "second-value"
    },
    "legend": {
      "text": "Have you changed your name?"
    }
  },
  "hint": {
    "text": "This includes changing your last name or spelling your name differently."
  },
  "items": [
    {
      "value": "yes",
      "text": "Yes"
    },
    {
      "value": "no",
      "text": "No",
      "checked": true
    }
  ]
}) }}
```

---

### Nunjucks arguments

The radios Nunjucks macro takes the following arguments:

| Name                | Type     | Required  | Description                 |
| --------------------|----------|-----------|-----------------------------|
| **fieldset**        | object   | No        | Arguments for the fieldset component (e.g. legend). See See [fieldset](https://github.com/wmp/wmp-frontend/tree/master/packages/components/fieldset) component. |
| **hint**            | object   | No        | Arguments for the hint component (e.g. text). See [hint](https://github.com/wmp/wmp-frontend/tree/master/packages/components/hint) component. |
| **errorMessage**    | object   | No        | Arguments for the errorMessage component (e.g. text). See [error message](https://github.com/wmp/wmp-frontend/tree/master/packages/components/error-message) component. |
| **idPrefix**        | string   | No        | String to prefix id for each radio item if no id is specified on each item. If`idPrefix` is not passed, fallback to using the name attribute instead.|
| **name**            | string	 | Yes       | Name attribute for each radio item. |
| **items**           | array    | Yes       | Array of radio item objects. |
| **items.{}.text (or) items.{}.html**       | string   | Yes        | Text or HTML to use within each radio item label. If `html` is provided, the `text` argument will be ignored. |
| **items.{}.id**     | string  | No        | Specific id attribute for the radio item. If omitted, then `idPrefix` string will be applied.|
| **items.{}.name**   | string  | Yes        | Specific name for the radio item. If omitted, then component global `name` string will be applied. |
| **items.{}.value**  | string   | Yes        | Value for the radio input. |
| **items.{}.hint**   | object   | No        | Provide optional hint to each radio item. See [hint](https://github.com/wmp/wmp-frontend/tree/master/packages/components/hint) component. |
| **items.{}.divider** | string   | No        | Optional divider text to separate radio items, for example the text "or". |
| **items.{}.checked** | boolean   | No        | If true, radio will be checked. |
| **items.{}.disabled** | boolean   | No        | If true, radio will be disabled. |
| **items.{}.attributes** | object   | No        | Any extra HTML attributes (for example data attributes) to add to the radio input tag. |
| **classes**         | string   | No        | Optional additional classes to add to the radios container. Separate each class with a space. |
| **attributes**      | object   | No        | Any extra HTML attributes (for example data attributes) to add to the radios container. |

If you are using Nunjucks macros in production be aware that using `html` arguments, or ones ending with `html` can be a [security risk](https://developer.mozilla.org/en-US/docs/Glossary/Cross-site_scripting). Read more about this in the [Nunjucks documentation](https://mozilla.github.io/nunjucks/api.html#user-defined-templates-warning).

## Thanks to the Government Digital Service (GDS)

This component and documentation has been taken from [GOV.UK Frontend - Radios component](https://github.com/alphagov/govuk-frontend/tree/master/package/components/radios) with a few minor adaptations.
