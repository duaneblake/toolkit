// Components
// import wmp_feedbackBanner from './components/feedback-banner/feedback-banner';
// import wmp_header from './components/header/header';
import wmp_skipLink from './components/skip-link/skip-link';
// import autocomplete from './components/header/autocomplete';

import wmp_header from './alton/components/header/header';
import wmp_tabs from './alton/components/tabs/tabs';
// HTML5 polyfills
import './alton/components/details/details.polyfill';

// Initialize components
document.addEventListener('DOMContentLoaded', function() {
  // wmp_feedbackBanner(3000);
  // wmp_header();
  wmp_skipLink();
  wmp_header();
  // autocomplete();
  wmp_tabs();
})
