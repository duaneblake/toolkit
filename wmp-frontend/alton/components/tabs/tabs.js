/*
 * Tabs
 *
 * Tabs on the site
 * 
 */

/**
 * Creates the tabs for the site
 * @param {*} e 
 */
function findTab(e) {

    //Gets the current screensize
    var windowWidth = window.screen.width;

    //If the windowwidth is more 769px set the tabs prcoess
    if (windowWidth >= 769 ) {


        e.preventDefault();

        //Adds class to say we using tabs
        document.body.classList.add('wmp-tabs-on');

        //Gets the parent ID incase there is multiple tabs on the page
        let parentTab   = e.currentTarget.closest('.wmp-tabs__list');
        let parentTabName   = parentTab.getAttribute("id");
        let tabContent  = document.getElementById(parentTabName+"Holder").querySelectorAll(".wmp-tabs__panel");
        let tabsItem    = document.getElementById(parentTabName).querySelectorAll(".wmp-tabs__tab");
        let tabsCurrent  = e.currentTarget;
        console.log(tabsCurrent);

        //Removed the arrow keys to fix at a later date
        // parentTab.addEventListener('keydown', function (evt) {
        //     let tabsUpdated = tabsCurrent.parentNode.previousElementSibling.firstElementChild;
        //     changeTabs(tabsItem, tabContent, tabsUpdated);
        // })
        
        changeTabs(tabsItem, tabContent, tabsCurrent)

    }

}

/**
 * Changes the tabs
 * @param {*} tabsItem 
 * @param {*} tabContent 
 * @param {*} tabsCurrent 
 */
function changeTabs(tabsItem, tabContent, tabsCurrent) {
        //Remove active links of the tabs and the aria selected tabs
        tabsItem.forEach(item => {
            item.classList.remove('wmp-tabs__tab--selected');
            item.setAttribute('aria-selected', 'false');
        })

        //Remove active classes and aria tags
        tabContent.forEach(item => {
            item.classList.remove('wmp-tabs__panel--selected');
            item.setAttribute('aria-hidden', 'true');
        });
        

        //Activates the class
        tabsCurrent.setAttribute('aria-selected', 'true');
        tabsCurrent.classList.add('wmp-tabs__tab--selected');

        let tabName = tabsCurrent.getAttribute('href').replace("#", "");
        
        document.getElementById(tabName).classList.add("wmp-tabs__panel--selected");
        document.getElementById(tabName).setAttribute('aria-hidden', 'false');
}


function keyThroughTabs(){

    const tabList = document.querySelector('[role="tablist"]');
    const tabs = document.querySelectorAll('[role="tab"]');

    tabList.addEventListener("keydown", e =>  {
        if (e.keyCode === 37) {
            let currentTab = tabList.querySelector('.wmp-tabs__tab--selected');
            let previousTab = currentTab.parentNode.previousSibling.currentTarget;
            let previousTaba = currentTab.parentNode;
            console.log('current:' + currentTab);
            console.log(previousTab);
        } else {

        }
    });

}

    
function selectTab(){
    let tabLinks = document.querySelectorAll('.wmp-tabs__tab');
    tabLinks.forEach(tabLink => tabLink.addEventListener('click', findTab));

}

function wmp_tabs(){
    selectTab();
    // keyThroughTabs();
} 

export default wmp_tabs;