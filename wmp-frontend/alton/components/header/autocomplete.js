// function autocomplete(params) {
  const endpoint = '/sample/data.json';
  const results = [];

  fetch(endpoint)
    .then(blob => blob.json())
    .then(data => results.push(...data));

  /**
   * 
   * @param {*} wordToMatch 
   * @param {*} results 
   */
  function findMatches(wordToMatch, results) {
    let count = 0;
    if (wordToMatch == ''){
      results = [];
    }

    return results.filter(result => {
      const regex = new RegExp(wordToMatch, 'gi');
      return (result.title.match(regex) || result.keywords.match(regex) || (result.description.match(regex))) && count++ < 5;
    });
  }
// }

/**
 * 
 */
function displayResults(){
  const matchArray = findMatches(this.value, results);
  let html = matchArray.map(result => {
    return `
    <li class="wmp-header__search-results--item">
      <a class="wmp-header__search-results--link"  href="${result.url}">
        <span class="wmp-header__search-results--title">${result.title}</span>
        <span class="wmp-header__search-results--description">${result.description}</span>
      </a>
    </li>
  `;
  }).join('');

  //Show if search result return no value
  if ((this.value != '') && (matchArray === undefined || matchArray.length == 0)){
    html =  `
      <li class="wmp-header__search-results--item">
        <a class="wmp-header__search-results--link"  href="https://www.west-midlands.police.uk/advice">
          <span class="wmp-header__search-results--title">Sorry, we can't seem to find what you're looking for.</span>
          <span class="wmp-header__search-results--description">Please click here for a full list of our advice pages.</span>
        </a>
      </li>
    `;
  }

  resultsPanel = this.getAttribute('data-search');

  document.getElementById(resultsPanel).innerHTML = html;
}

const searchInput   = document.getElementById('search-field');
const searchResults = document.getElementById('search-results--desktop');

// searchInput.addEventListener('change', displayResults);
searchInput.addEventListener('keyup', displayResults);
searchInput.addEventListener('search', displayResults);


const searchInputMobile   = document.getElementById('search-field--mobile');
const searchResultsMobile = document.getElementById('search-results--mobile');
// searchInputMobile.addEventListener('change', displayResults);
searchInputMobile.addEventListener('keyup', displayResults);
searchInputMobile.addEventListener('search', displayResults);


// export default autocomplete;
